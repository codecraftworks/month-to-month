#! /usr/bin/env node
import admin from "firebase-admin";
import { markdownTable } from "markdown-table";

const serviceAccount = process.env.GOOGLE_APPLICATION_CREDENTIALS;
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

const db = admin.firestore();
const startDate = new Date(2018, 1, 1);
const now = new Date();
const headerRow = [
  "Year",
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

function getMonthsByYear() {
  let startYear = startDate.getFullYear();
  let endYear = now.getFullYear();
  let dates = {};
  for (let i = startYear; i <= endYear; i++) {
    dates[i] = {};
    let endMonth = i != endYear ? 11 : now.getMonth() - 1;
    let startMon = i === startYear ? startDate.getMonth() - 1 : 0;
    for (let j = startMon; j <= endMonth; j = j > 12 ? j % 12 || 11 : j + 1) {
      let month = j + 1;
      dates[i][month] = 0;
    }
  }
  return dates;
}

function runReport(collection, dateField = "created") {
  const collectionRef = db.collection(collection);
  collectionRef
    .get()
    .then(function (snapshot) {
      const dateCollection = getMonthsByYear();
      snapshot.forEach(function (doc) {
        if (doc.data()[dateField]) {
          const date =
            dateField === "created"
              ? doc.data()[dateField].toDate()
              : new Date(doc.data()[dateField]);
          const year = date.getFullYear();
          const month = date.getMonth() + 1;
          if (date >= startDate) {
            dateCollection[year][month] = dateCollection[year][month] + 1;
          }
        }
      });
      printData(collection, dateCollection, snapshot.size);
    })
    .catch((err) => {
      console.log(`Error getting ${collection} documents ${err}`);
    });
}

function printData(collectionName, collectionData, collectionSize) {
  const countRows = [headerRow];
  for (const year in collectionData) {
    const countRow = [year];
    for (const month in collectionData[year]) {
      countRow.push(collectionData[year][month]);
    }
    countRows.push(countRow);
  }
  console.log(`Total ${collectionName}: **${collectionSize}**\n`);
  console.log(`## ${collectionName} by Year/Month`);
  console.log(markdownTable(countRows));
  console.log("\n");
}

console.log(
  "# Month to Month Report for ",
  admin.app().options_.credential.projectId,
  "\n"
);
console.log("---\n");

runReport("users");

runReport("web-projects");

runReport("programs");

runReport("learndash", "post_date_gmt");

runReport("game-projects");
